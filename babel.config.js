module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: [
          '.js',
          '.jsx',
          '.ts',
          '.tsx',
          '.android.js',
          '.android.tsx',
          '.ios.js',
          '.ios.tsx',
          '.png',
          '.json',
          '.svg',
        ],
        alias: {
          assets: './src/assets',
          components: './src/components',
          navigations: './src/navigations',
          reducers: './src/reducers',
          screens: './src/screens',
          types: './src/types',
          constants: './src/constants',
          typings: './src/typings',
        },
      },
    ],
  ],
};

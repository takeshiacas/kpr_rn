import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import PublicNavigation from './PublicNavigation';

export type RootStackParamList = {
  PublicNavigation: undefined;
  OnboardingNavigation: undefined;
  PrivateNavigation: { onboardingCompleted: boolean };
};

const Stack = createStackNavigator<RootStackParamList>();

export default () => {
  return (
    <NavigationContainer
      linking={{
        prefixes: ['dating://', 'https://dating.com'],
        config: {
          screens: {
            PrivateNavigation: {
              screens: {
                ChooseInitialRouteScreen: {
                  path: '',
                },
              },
            },
          },
        },
      }}
    >
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="PublicNavigation" component={PublicNavigation} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import IntroScreen from 'screens/IntroScreen';
import { textSizes } from 'constants/theme';
import SignUpName from 'screens/SignUpNameScreen';
import SignUpPhoneNumber from 'screens/SignUpPhoneNumberScreen';
import SignUpBirthDate from '../screens/SignUpBirthDateScreen';
import IntroSettingIntentions from 'screens/IntroSettingIntentionsScreen';
import OptionPresence from 'screens/OptionPresenceScreen';
import OptionValue from 'screens/OptionValueScreen';
import OptionCommittedAction from 'screens/OptionCommittedActionScreen';
import OptionSelfAsContext from 'screens/OptionSelfAsContextScreen';
import OptionDefusion from 'screens/OptionDefusionScreen';
import OptionAcceptance from 'screens/OptionAcceptanceScreen';

export type PublicStackParamList = {
  IntroScreen: undefined;
  IntroSettingIntentions: undefined;
  SignUpName: undefined;
  SignUpPhoneNumber: undefined;
  SignUpBirthDate: undefined;
  OptionPresence: undefined;
  OptionValue: undefined;
  OptionCommittedAction: undefined;
  OptionSelfAsContext: undefined;
  OptionDefusion: undefined;
  OptionAcceptance: undefined;
};

export const TOTAL_STEPS = 10;
export const STEPS_MAP: Record<string, number> = {
  SignUpName: 1,
  SignUpPhoneNumber: 2,
  SignUpBirthDate: 3,
  OptionPresence: 4,
  OptionValue: 5,
  OptionCommittedAction: 6,
  OptionSelfAsContext: 7,
  OptionDefusion: 8,
  OptionAcceptance: 9,
};

const Stack = createStackNavigator<PublicStackParamList>();

export default () => (
  <Stack.Navigator
    screenOptions={() => ({
      title: '',
      headerTitleStyle: {
        fontSize: textSizes.md,
        fontFamily: 'Roboto',
        fontWeight: '500',
      },
      headerTitleAlign: 'left',
      cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      headerStyle: {
        elevation: 0,
        shadowOpacity: 0,
      },
    })}
    initialRouteName="IntroScreen"
    // initialRouteName="OptionDefusion"
  >
    <Stack.Screen
      options={{ headerShown: false }}
      name="IntroScreen"
      component={IntroScreen}
    />
    <Stack.Screen
      options={{ headerShown: false }}
      name="SignUpName"
      component={SignUpName}
    />
    <Stack.Screen
      options={{ headerShown: false }}
      name="SignUpPhoneNumber"
      component={SignUpPhoneNumber}
    />
    <Stack.Screen
      options={{ headerShown: false }}
      name="SignUpBirthDate"
      component={SignUpBirthDate}
    />
    <Stack.Screen
      options={{ headerShown: false }}
      name="IntroSettingIntentions"
      component={IntroSettingIntentions}
    />
    <Stack.Screen
      options={{ headerShown: false }}
      name="OptionPresence"
      component={OptionPresence}
    />
    <Stack.Screen
      options={{ headerShown: false }}
      name="OptionValue"
      component={OptionValue}
    />
    <Stack.Screen
      options={{ headerShown: false }}
      name="OptionCommittedAction"
      component={OptionCommittedAction}
    />
    <Stack.Screen
      options={{ headerShown: false }}
      name="OptionSelfAsContext"
      component={OptionSelfAsContext}
    />
    <Stack.Screen
      options={{ headerShown: false }}
      name="OptionDefusion"
      component={OptionDefusion}
    />
    <Stack.Screen
      options={{ headerShown: false }}
      name="OptionAcceptance"
      component={OptionAcceptance}
    />
  </Stack.Navigator>
);

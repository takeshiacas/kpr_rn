import { combineReducers, configureStore } from '@reduxjs/toolkit';
import onboarding from './onboarding';
export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;

const rootReducer = combineReducers({
  onboarding,
});

const store = configureStore({
  reducer: rootReducer,
});

export default store;

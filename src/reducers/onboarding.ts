import { createReducer, createAction, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from './index';

export interface OnboardingFormState {
  name: string;
  phoneNumber: string;
  birthDate: Date;
  isClientReady: boolean;
}

interface State {
  data?: OnboardingFormState;
  status?: string;
  error?: any;
}

export const initialState: State = {
  data: {
    name: '',
    phoneNumber: '',
    birthDate: new Date(),
    isClientReady: false,
  },
  status: 'pending',
  error: null,
};

export const onboardingFormSelector = ({
  onboarding,
}: RootState): OnboardingFormState => onboarding.data as OnboardingFormState;

export const updateOnboardingProccess = createAction<
  Partial<OnboardingFormState>,
  'onboarding-options/update'
>('onboarding-options/update');

export default createReducer(initialState, {
  [updateOnboardingProccess.type]: (
    state: State,
    action: PayloadAction<OnboardingFormState>
  ) => {
    return {
      ...state,
      data: {
        ...state.data,
        ...action.payload,
      },
    };
  },
});

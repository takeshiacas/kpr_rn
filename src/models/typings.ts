export type TextAlignTypes = 'left' | 'center' | 'right' | 'justify';

export type FontWeightTypes =
  | 'normal'
  | 'bold'
  | '100'
  | '200'
  | '300'
  | '400'
  | '500'
  | '600'
  | '700'
  | '800'
  | '900';

export type TextTransformTypes = 'capitalize' | 'uppercase' | 'lowercase';
export type SizeTypes = 'xs' | 'sm' | 'md' | 'lg' | 'xl' | 'xxl';
export type TextDecorationLine =
  | 'none'
  | 'underline'
  | 'line-through'
  | 'underline line-through';
export type VisibilityType = 'visible' | 'hidden';

import React from 'react';
import {
  TouchableOpacity,
  TouchableOpacityProps,
  ViewStyle,
} from 'react-native';

export interface TouchableProps
  extends Pick<
      TouchableOpacityProps,
      | 'onPress'
      | 'onPressIn'
      | 'onPressOut'
      | 'onLongPress'
      | 'activeOpacity'
      | 'disabled'
    >,
    ViewStyle {
  children: React.ReactNode;
  hitSlop?: number;
  testID?: string;
  accessibilityLabel?: string;
}

const Touchable = ({
  children,
  onPress,
  onPressIn,
  onPressOut,
  onLongPress,
  hitSlop,
  disabled,
  activeOpacity,
  testID,
  accessibilityLabel,
  ...style
}: TouchableProps) => (
  <TouchableOpacity
    testID={testID}
    accessibilityLabel={accessibilityLabel}
    style={style}
    disabled={disabled}
    activeOpacity={activeOpacity}
    hitSlop={{ top: hitSlop, right: hitSlop, bottom: hitSlop, left: hitSlop }}
    onPress={onPress}
    onPressIn={onPressIn}
    onPressOut={onPressOut}
    onLongPress={onLongPress}
    delayLongPress={0}
  >
    {children}
  </TouchableOpacity>
);

Touchable.defaultProps = {
  activeOpacity: 0.8,
} as Partial<TouchableProps>;

export default Touchable;

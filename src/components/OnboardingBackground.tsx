import styled, { css } from 'styled-components/native';
import Background from '../assets/images/backgroung.svg';
import { Dimensions } from 'react-native';

export const OnboardingBackground = styled(Background)`
  position: absolute;
  width: ${Dimensions.get('window').width}px;
  height: ${Dimensions.get('window').height}px;
  ${(props) =>
    props.top &&
    css`
      top: ${props.top};
    `}
  ${(props) =>
    props.bottom &&
    css`
      bottom: ${props.bottom};
    `}
`;

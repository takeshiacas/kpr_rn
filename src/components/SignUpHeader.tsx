import React from 'react';
import { StyleSheet } from 'react-native';
import { colors } from 'constants/theme';
import SignUpStepper, { SignUpStepperProps } from './SignUpStepper';
import View from './View';
import { Text } from 'react-native-elements';

export interface SignUpHeaderProps extends SignUpStepperProps {}

const SignUpHeader = ({ navigation, current, total }: SignUpHeaderProps) => {
  return (
    <View flexDirection="row">
      <View grow>
        <SignUpStepper
          total={total}
          current={current}
          navigation={navigation}
        />
      </View>
      <Text style={styles.titleStyle}>ABOUT YOU</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  titleStyle: {
    fontFamily: 'Poppins',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 24,
    color: colors.blue100,
  },
});

export default SignUpHeader;

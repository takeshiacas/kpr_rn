import React from 'react';
import {
  View as RNView,
  ViewProps as RNViewProps,
  ViewStyle,
} from 'react-native';
import { SizeTypes } from 'models/typings';
import { spacingSizes } from 'constants/theme';

const getSpacingBySize = (size: SizeTypes | undefined) =>
  size ? spacingSizes[size] : undefined;

export interface ViewProps extends ViewStyle {
  children?: React.ReactNode | React.ReactNode[];
  margin?: SizeTypes;
  marginHorizontal?: SizeTypes | number;
  marginVertical?: SizeTypes | number;
  padding?: SizeTypes;
  paddingHorizontal?: SizeTypes;
  paddingVertical?: SizeTypes;
  props?: RNViewProps;
  grow?: boolean;
  shrink?: boolean;
  animated?: boolean;
}

const View = React.forwardRef(
  (
    {
      children,
      margin,
      marginHorizontal,
      marginVertical,
      padding,
      paddingHorizontal,
      paddingVertical,
      props,
      grow,
      shrink,
      animated,
      ...styles
    }: ViewProps,
    ref: any
  ) => {
    const marginSize = getSpacingBySize(margin);
    const marginHorizontalSize =
      typeof marginHorizontal === 'number'
        ? marginHorizontal
        : getSpacingBySize(marginHorizontal);
    const marginVerticalSize =
      typeof marginVertical === 'number'
        ? marginVertical
        : getSpacingBySize(marginVertical);
    const paddingSize = getSpacingBySize(padding);
    const paddingHorizontalSize = getSpacingBySize(paddingHorizontal);
    const paddingVerticalSize = getSpacingBySize(paddingVertical);
    const flexGrow = grow ? 1 : 0;
    const flexShrink = shrink ? 1 : 0;

    return React.createElement(
      RNView,
      {
        ref,
        ...props,
        style: {
          ...styles,
          marginHorizontal: marginHorizontalSize,
          marginVertical: marginVerticalSize,
          margin: marginSize,
          paddingHorizontal: paddingHorizontalSize,
          paddingVertical: paddingVerticalSize,
          padding: paddingSize,
          flexGrow,
          flexShrink,
        },
      },
      children
    );
  }
);

export default View;

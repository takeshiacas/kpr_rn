import React from 'react';
import { Text as RNText, I18nManager } from 'react-native';
import { colors, headingSizes } from 'constants/theme';
import { TextProps } from './Text';

export interface HeadingProps extends TextProps {
  letterSpacing?: number;
  lineHeight?: number;
}

const Heading = ({
  children,
  size,
  color = 'blue100',
  textAlign,
  fontWeight,
  textTransform,
  textDecorationLine,
  lineHeight = 28,
  letterSpacing,
  fontFamily,
}: HeadingProps) => {
  const colorVal = colors[color];

  return (
    <RNText
      style={{
        fontSize: headingSizes[size!],
        color: colorVal,
        textAlign,
        fontWeight,
        textTransform,
        fontFamily,
        textDecorationLine,
        lineHeight,
        letterSpacing,
        writingDirection: I18nManager.isRTL ? 'rtl' : 'ltr',
      }}
    >
      {children}
    </RNText>
  );
};

Heading.defaultProps = {
  size: 'md',
  fontWeight: '500',
} as Partial<HeadingProps>;

export default Heading;

import React from 'react';
import { Button } from 'react-native-elements';
import { StyleSheet, FlatList } from 'react-native';
import { colors } from '../constants/theme';

export interface SignUpStepperProps {
  total: number;
  current: number;
  navigation: any;
}

const onPressMap = [
  (navigation: any) => () => navigation.navigate('SignUpName'),
  (navigation: any) => () => navigation.navigate('SignUpPhoneNumber'),
  (navigation: any) => () => navigation.navigate('SignUpBirthDate'),
  (navigation: any) => () => navigation.navigate('IntroSettingIntentions'),
];

const SignUpStepper = ({ total, current, navigation }: SignUpStepperProps) => {
  const data = new Array(total).fill(0);
  return (
    <FlatList
      horizontal
      data={data}
      renderItem={({ index: position }) => {
        return (
          <Button
            buttonStyle={styles.buttonStyle}
            title=""
            type="solid"
            disabled={current <= position}
            onPress={
              onPressMap[position] ? onPressMap[position](navigation) : () => {}
            }
          />
        );
      }}
      keyExtractor={(item, index) => `${index}`}
    />
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    borderRadius: 8,
    width: 17,
    height: 17,
    backgroundColor: colors.purple300,
    marginLeft: 8,
  },
});

export default SignUpStepper;

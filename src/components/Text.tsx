import React from 'react';
import {
  Text as RNText,
  GestureResponderEvent,
  I18nManager,
} from 'react-native';
import {
  SizeTypes,
  TextAlignTypes,
  FontWeightTypes,
  TextTransformTypes,
  TextDecorationLine,
} from 'models/typings';
import { textSizes, colors, lineHeights, ThemesFonts } from 'constants/theme';

export interface TextProps {
  children: React.ReactNode;
  size?: SizeTypes;
  color?: keyof typeof colors | undefined;
  textAlign?: TextAlignTypes;
  fontWeight?: FontWeightTypes;
  textTransform?: TextTransformTypes;
  textDecorationLine?: TextDecorationLine;
  onPress?: (event: GestureResponderEvent) => void;
  testID?: string;
  accessibilityLabel?: string;
  writingDirection?: 'ltr' | 'rtl' | undefined;
  fontFamily?: ThemesFonts | undefined;
}

const Text = ({
  children,
  size = 'md',
  color = 'blue100',
  textAlign,
  fontWeight,
  textTransform,
  textDecorationLine,
  onPress,
  testID,
  accessibilityLabel,
  writingDirection,
  fontFamily,
}: TextProps) => {
  const colorVal = colors[color];

  return (
    <RNText
      testID={testID}
      accessibilityLabel={accessibilityLabel}
      style={{
        fontSize: textSizes[size!],
        lineHeight: lineHeights[size!],
        color: colorVal,
        textAlign,
        fontWeight,
        textTransform,
        fontFamily,
        textDecorationLine,
        writingDirection: writingDirection
          ? writingDirection
          : I18nManager.isRTL
          ? 'rtl'
          : 'ltr',
      }}
      onPress={onPress}
    >
      {children}
    </RNText>
  );
};

export default Text;

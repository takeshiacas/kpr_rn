import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Slider as RNESlider } from 'react-native-elements';
import Text from './Text';
import { colors } from 'constants/theme';

export interface SliderProps {
  leftText: string;
  rightText: string;
}

const Slider = ({ leftText, rightText }: SliderProps) => {
  const handlerChange = (value: number) => {
    console.log('value', value);
  };

  return (
    <View style={styles.container}>
      <View style={styles.topContainer}>
        <View style={styles.textContainer}>
          <Text>{leftText}</Text>
        </View>
        <View style={styles.textContainer}>
          <Text>{rightText}</Text>
        </View>
      </View>
      <View style={styles.sliderContanier}>
        <RNESlider
          minimumValue={0}
          maximumValue={4}
          value={2}
          step={1}
          minimumTrackTintColor={colors.blue100}
          maximumTrackTintColor={colors.blue100}
          trackStyle={styles.sliderTrack}
          thumbStyle={styles.sliderThumb}
          onSlidingComplete={handlerChange}
        />
      </View>
    </View>
  );
};

export default Slider;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'flex-end',
    marginTop: 10,
  },
  topContainer: {
    marginVertical: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textContainer: {
    marginBottom: 10,
  },
  sliderContanier: {
    paddingHorizontal: 40,
  },
  sliderTrack: {
    height: 3,
    backgroundColor: colors.blue600,
  },
  sliderThumb: {
    height: 20,
    width: 20,
    backgroundColor: colors.blue600,
  },
});

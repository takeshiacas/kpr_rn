import React from 'react';
import { StyleSheet } from 'react-native';
import { colors } from 'constants/theme';
import ReflectionStepper, { ReflectionStepperProps } from './ReflectionStepper';
import View from './View';
import { Text } from 'react-native-elements';

export interface ReflectionHeaderProps extends ReflectionStepperProps {}

const ReflectionHeader = ({ navigation, current, total }: ReflectionStepperProps) => {
  return (
    <View flexDirection="row">
      <View grow>
        <ReflectionStepper
          total={total}
          current={current}
          navigation={navigation}
        />
      </View>
      <Text style={styles.titleStyle}>REFLECTION</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  titleStyle: {
    fontFamily: 'Poppins',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 24,
    color: colors.blue100,
  },
});

export default ReflectionHeader;

import React from 'react';
import { Button } from 'react-native-elements';
import { StyleSheet, FlatList } from 'react-native';
import { colors } from '../constants/theme';

export interface ReflectionStepperProps {
  total: number;
  current: number;
  navigation: any;
}

const onPressMap = [
  (navigation: any) => () => navigation.navigate('Presence'),
  (navigation: any) => () => navigation.navigate('Value'),
  (navigation: any) => () => navigation.navigate('CommitedAction'),
  (navigation: any) => () => navigation.navigate('SelfAsContext'),
  (navigation: any) => () => navigation.navigate('Defusion'),
  (navigation: any) => () => navigation.navigate('Acceptance'),
];

const ReflectionStepper = ({ total, current, navigation }: ReflectionStepperProps) => {
  const data = new Array(total).fill(0);
  return (
    <FlatList
      horizontal
      data={data}
      renderItem={({ index: position }) => {
        return (
          <Button
            buttonStyle={styles.buttonStyle}
            title=""
            type="solid"
            disabled={current <= position}
            onPress={
              onPressMap[position] ? onPressMap[position](navigation) : () => {}
            }
          />
        );
      }}
      keyExtractor={(item, index) => `${index}`}
    />
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    borderRadius: 8,
    width: 17,
    height: 17,
    backgroundColor: colors.purple300,
    marginLeft: 8,
  },
});

export default ReflectionStepper;

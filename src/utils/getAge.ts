export const getAge = (dateString: Date) => {
  const today = new Date();
  let age = today.getFullYear() - dateString.getFullYear();
  const m = today.getMonth() - dateString.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < dateString.getDate())) {
    age--;
  }
  return age;
};

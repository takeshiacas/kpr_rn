export const colors = {
  light: '#ffffff',
  blue100: '#849DEB',
  blue600: '#091BBB',
  blue900: '#02006A',
  purple300: '#849DEB',
  dark: '#263238',
  primary100: '#e43534',
  green100: '#113100',
  orange100: '#fb8c00',
  black300: '#33302C',
};

export const headingSizes = {
  xs: 18,
  sm: 21,
  md: 24,
  lg: 28,
  xl: 32,
  xxl: 36,
};

export const textSizes = {
  xs: 12,
  sm: 14,
  md: 16,
  lg: 18,
  xl: 20,
  xxl: 30,
};

export const lineHeights = {
  xs: 18,
  sm: 20,
  md: 22,
  lg: 24,
  xl: 26,
  xxl: 30,
};

export const spacingSizes = {
  xs: 5,
  sm: 10,
  md: 15,
  lg: 23,
  xl: 50,
  xxl: 120,
};

export type ThemesFonts =
  | 'Fontisto'
  | 'Octicons'
  | 'Roboto-Medium'
  | 'Feather'
  | 'Entypo'
  | 'FontAwesome5_Brands'
  | 'MaterialCommunityIcons'
  | 'Roboto-Regular'
  | 'Optima-Medium.woff'
  | 'Poppins-ExtraLight'
  | 'Poppins-ThinItalic'
  | 'optima-bold'
  | 'Poppins-ExtraLightItalic'
  | 'Poppins-Light'
  | 'Poppins-BoldItalic'
  | 'AntDesign'
  | 'Poppins-Medium'
  | 'Foundation'
  | 'Ionicons'
  | 'FontAwesome5_Solid'
  | 'Poppins-SemiBoldItalic'
  | 'FontAwesome5_Regular'
  | 'OpenSans-Italic'
  | 'FontAwesome'
  | 'Poppins-ExtraBoldItalic'
  | 'Zocial'
  | 'EvilIcons'
  | 'OpenSans-Bold'
  | 'Poppins-LightItalic'
  | 'Poppins-BlackItalic'
  | 'Roboto-Italic'
  | 'Poppins-Bold'
  | 'SimpleLineIcons'
  | 'OpenSans-Regular'
  | 'unicode.optima'
  | 'Poppins-Regular'
  | 'Poppins-Black'
  | 'OpenSans-BoldItalic'
  | 'Roboto-Bold'
  | 'Poppins-Thin'
  | 'Poppins-ExtraBold'
  | 'Poppins-SemiBold'
  | 'Poppins-Italic'
  | 'MaterialIcons'
  | 'Poppins-MediumItalic';

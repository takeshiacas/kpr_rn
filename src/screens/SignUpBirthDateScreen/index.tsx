import React, { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import {
  PublicStackParamList,
  STEPS_MAP,
  TOTAL_STEPS,
} from 'navigations/PublicNavigation';
import { colors } from 'constants/theme';
import View from 'components/View';
import { StatusBar, StyleSheet, Text } from 'react-native';
import Heading from 'components/Heading';
import { Button } from 'react-native-elements';
import { OnboardingBackground } from 'components/OnboardingBackground';
import { Formik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import {
  onboardingFormSelector,
  OnboardingFormState,
  updateOnboardingProccess,
} from 'reducers/onboarding';
import DatePicker from 'react-native-date-picker';
import { getAge } from '../../utils/getAge';
import SignUpHeader from 'components/SignUpHeader';
import { RootState } from 'reducers';

type ScreenNavigationProp = StackNavigationProp<
  PublicStackParamList,
  'SignUpBirthDate'
>;

type Props = {
  navigation: ScreenNavigationProp;
};

const SignUpBirthDate = ({ navigation }: Props) => {
  const dispatch = useDispatch();
  const [allowNext, setAllowNext] = useState(false);
  const { birthDate } = useSelector<RootState, OnboardingFormState>(
    onboardingFormSelector
  );

  const minimumDate = new Date();
  minimumDate.setFullYear(minimumDate.getFullYear() - 18);

  return (
    <View
      backgroundColor={colors.light}
      padding="lg"
      alignItems="stretch"
      justifyContent="flex-start"
      flex={1}
      grow
    >
      <StatusBar backgroundColor={colors.light} barStyle="light-content" />
      <OnboardingBackground bottom={'-90px'} />
      <View height={100} marginTop={20}>
        <SignUpHeader
          current={STEPS_MAP[SignUpBirthDate.name]}
          navigation={navigation}
          total={TOTAL_STEPS}
        />
      </View>

      <Formik
        initialValues={{ birthDate }}
        onSubmit={(values: Partial<OnboardingFormState>) => {
          dispatch(
            updateOnboardingProccess({
              birthDate: values.birthDate,
            })
          );
          navigation.navigate('OptionPresence');
        }}
      >
        {({ touched, errors, setFieldValue, handleSubmit, values }) => (
          <>
            <View alignItems="center" justifyContent="center">
              <Heading
                size="xxl"
                fontWeight="700"
                fontFamily="optima-bold"
                color="blue900"
                lineHeight={44}
              >
                {'What’s your date of \n birth?'}
              </Heading>
              <DatePicker
                date={values.birthDate}
                mode="date"
                maximumDate={minimumDate}
                textColor={colors.blue900}
                onDateChange={(date) => {
                  setAllowNext(true);
                  setFieldValue('birthDate', date);
                }}
              />
              {errors.birthDate && touched.birthDate ? (
                <Text>{errors.birthDate}</Text>
              ) : null}

              {allowNext && values.birthDate && values.birthDate ? (
                <Text style={styles.calculateAgeTextStyle}>
                  Age {getAge(values.birthDate)}
                </Text>
              ) : null}
            </View>
            <View
              alignItems="flex-end"
              justifyContent="flex-end"
              marginTop={40}
            >
              <Button
                buttonStyle={styles.buttonStyle}
                titleStyle={styles.buttonTitleStyle}
                title="next"
                type="solid"
                disabled={!allowNext}
                onPress={() => {
                  handleSubmit();
                }}
              />
            </View>
          </>
        )}
      </Formik>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    borderRadius: 32,
    paddingHorizontal: 48,
    paddingVertical: 12,
    backgroundColor: colors.purple300,
  },
  inputStyle: {
    borderBottomColor: colors.blue600,
    borderBottomWidth: 1,
    fontFamily: 'Poppins',
    fontWeight: '500',
    fontSize: 34,
    lineHeight: 48,
    color: colors.blue900,
  },
  birthDatePrefixStyle: {
    fontFamily: 'Poppins',
    fontWeight: '500',
    fontSize: 34,
    lineHeight: 51,
    color: colors.blue900,
  },
  buttonTitleStyle: { fontFamily: 'Poppins', fontSize: 16, lineHeight: 24 },
  calculateAgeTextStyle: {
    fontFamily: 'Poppins',
    fontSize: 18,
    lineHeight: 24,
    color: colors.blue900,
  },
});

export default SignUpBirthDate;

import React from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import {
  PublicStackParamList,
  STEPS_MAP,
  TOTAL_STEPS,
} from 'navigations/PublicNavigation';
import { colors } from 'constants/theme';
import View from 'components/View';
import { StatusBar, StyleSheet, Text } from 'react-native';
import Heading from 'components/Heading';
import { Button } from 'react-native-elements';
import { OnboardingBackground } from 'components/OnboardingBackground';
import { Formik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import {
  onboardingFormSelector,
  OnboardingFormState,
  updateOnboardingProccess,
} from 'reducers/onboarding';
import { Input } from 'react-native-elements';
import { PhoneSchema } from './formValidation';
import SignUpHeader from 'components/SignUpHeader';
import { RootState } from 'reducers';

type ScreenNavigationProp = StackNavigationProp<
  PublicStackParamList,
  'SignUpPhoneNumber'
>;

type Props = {
  navigation: ScreenNavigationProp;
};

const SignUpPhoneNumber = ({ navigation }: Props) => {
  const dispatch = useDispatch();
  const { phoneNumber } = useSelector<RootState, OnboardingFormState>(
    onboardingFormSelector
  );
  return (
    <View
      backgroundColor={colors.light}
      padding="lg"
      alignItems="stretch"
      justifyContent="center"
      flex={1}
      grow
    >
      <StatusBar backgroundColor={colors.light} barStyle="light-content" />
      <OnboardingBackground bottom={'-90px'} />
      <View height={100} marginTop={20}>
        <SignUpHeader
          current={STEPS_MAP[SignUpPhoneNumber.name]}
          navigation={navigation}
          total={TOTAL_STEPS}
        />
      </View>
      <Formik
        initialValues={{ phoneNumber }}
        validationSchema={PhoneSchema}
        onSubmit={(values: Partial<OnboardingFormState>) => {
          dispatch(
            updateOnboardingProccess({
              phoneNumber: values.phoneNumber,
            })
          );
          navigation.navigate('SignUpBirthDate');
        }}
      >
        {({
          touched,
          errors,
          handleChange,
          handleBlur,
          handleSubmit,
          values,
        }) => (
          <>
            <View alignItems="center" justifyContent="center" flex={1} grow>
              <Heading
                size="xxl"
                fontWeight="700"
                fontFamily="optima-bold"
                color="blue900"
                lineHeight={44}
              >
                What’s your phone number?
              </Heading>
              <View
                flexDirection="row"
                alignItems="baseline"
                justifyContent="center"
              >
                <Text style={styles.phoneNumberPrefixStyle}>+1</Text>
                <View grow>
                  <Input
                    onChangeText={handleChange('phoneNumber')}
                    onBlur={handleBlur('phoneNumber')}
                    value={values.phoneNumber}
                    inputStyle={styles.inputStyle}
                    style={{ marginTop: 60 }}
                    keyboardType="numeric"
                  />
                  {errors.phoneNumber && touched.phoneNumber ? (
                    <Text>{errors.phoneNumber}</Text>
                  ) : null}
                </View>
              </View>
            </View>
            <View
              grow
              alignItems="flex-end"
              justifyContent="flex-end"
              flex={1}
              marginBottom={40}
            >
              <Button
                buttonStyle={styles.buttonStyle}
                titleStyle={styles.buttonTitleStyle}
                title="next"
                type="solid"
                onPress={() => {
                  handleSubmit();
                }}
              />
            </View>
          </>
        )}
      </Formik>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    borderRadius: 32,
    paddingHorizontal: 48,
    paddingVertical: 12,
    backgroundColor: colors.purple300,
  },
  inputStyle: {
    borderBottomColor: colors.blue600,
    borderBottomWidth: 1,
    fontFamily: 'Poppins',
    fontWeight: '500',
    fontSize: 34,
    lineHeight: 48,
    color: colors.blue900,
  },
  phoneNumberPrefixStyle: {
    fontFamily: 'Poppins',
    fontWeight: '500',
    fontSize: 34,
    lineHeight: 51,
    color: colors.blue900,
  },
  buttonTitleStyle: { fontFamily: 'Poppins', fontSize: 16, lineHeight: 24 },
});

export default SignUpPhoneNumber;

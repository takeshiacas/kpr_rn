import * as Yup from 'yup';

export const NameSchema = Yup.object().shape({
  name: Yup.string().min(2, 'Too short!').max(50, 'Too long!').required(),
});

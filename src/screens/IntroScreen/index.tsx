import React from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { PublicStackParamList } from 'navigations/PublicNavigation';
import { colors } from 'constants/theme';
import View from 'components/View';
import { StatusBar, StyleSheet } from 'react-native';
import Heading from 'components/Heading';
import { Button } from 'react-native-elements';
import { OnboardingBackground } from 'components/OnboardingBackground';

type ScreenNavigationProp = StackNavigationProp<
  PublicStackParamList,
  'IntroScreen'
>;

type Props = {
  navigation: ScreenNavigationProp;
};

const IntroScreen = ({ navigation }: Props) => {
  return (
    <View
      backgroundColor={colors.light}
      padding="lg"
      alignItems="stretch"
      justifyContent="center"
      flex={1}
      grow
    >
      <StatusBar backgroundColor={colors.light} barStyle="light-content" />
      <OnboardingBackground bottom={'30px'} />

      <View alignItems="flex-start" justifyContent="center" flex={1} grow>
        <Heading
          size="sm"
          fontWeight="400"
          fontFamily="Poppins-Light"
          color="blue100"
          lineHeight={HeadingLineHeight}
        >
          {' BE AUTHENTIC'}
        </Heading>
        <Heading
          size="sm"
          fontWeight="400"
          fontFamily="Poppins-Light"
          color="blue100"
          lineHeight={HeadingLineHeight}
        >
          {'GROW SELF-AWARENESS'}
        </Heading>
        <Heading
          size="sm"
          fontWeight="400"
          fontFamily="Poppins-Light"
          color="blue100"
          lineHeight={HeadingLineHeight}
        >
          {'CONNECT DEEPLY \n WELCOME TO'}{' '}
          <Heading size="md" fontFamily="OpenSans-Bold" color="blue900">
            KEEPLER
          </Heading>
        </Heading>
      </View>
      <View
        grow
        alignItems="center"
        justifyContent="flex-end"
        flex={1}
        marginBottom={40}
      >
        <Button
          buttonStyle={styles.buttonStyle}
          titleStyle={styles.buttonTitleStyle}
          title="I'm ready"
          type="solid"
          onPress={() => navigation.navigate('SignUpName')}
        />
      </View>
    </View>
  );
};

const HeadingLineHeight = 70;
const styles = StyleSheet.create({
  buttonStyle: {
    borderRadius: 32,
    paddingHorizontal: 48,
    paddingVertical: 12,
    backgroundColor: colors.purple300,
  },
  buttonTitleStyle: { fontFamily: 'Poppins', fontSize: 16, lineHeight: 24 },
});

export default IntroScreen;

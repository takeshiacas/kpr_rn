import React from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { PublicStackParamList } from 'navigations/PublicNavigation';
import { colors } from 'constants/theme';
import View from 'components/View';
import { StatusBar, StyleSheet } from 'react-native';
import Heading from 'components/Heading';
import { Button, Text } from 'react-native-elements';
import { OnboardingBackground } from 'components/OnboardingBackground';
import { updateOnboardingProccess } from 'reducers/onboarding';
import { useDispatch } from 'react-redux';

type ScreenNavigationProp = StackNavigationProp<
  PublicStackParamList,
  'IntroSettingIntentions'
>;

type Props = {
  navigation: ScreenNavigationProp;
};

const IntroSettingIntentions = ({ navigation }: Props) => {
  const dispatch = useDispatch();
  return (
    <View
      backgroundColor={colors.light}
      paddingHorizontal="lg"
      paddingVertical="sm"
      alignItems="center"
      justifyContent="center"
      flex={1}
      grow
    >
      <StatusBar backgroundColor={colors.light} barStyle="light-content" />
      <OnboardingBackground bottom={'-120px'} />

      <View alignItems="flex-start" justifyContent="center">
        <Heading
          size="xxl"
          fontWeight="700"
          fontFamily="optima-bold"
          color="blue900"
          lineHeight={44}
        >
          Be intentional when it comes to dating.
        </Heading>
        <Text style={styles.descriptionStyle}>
          Setting intentions helps you to stay aligned with your goals and
          values. We walk you through a short exercise to get clear on why
          you’re here and what you’re looking for.
        </Text>
      </View>
      <View
        alignItems="flex-start"
        justifyContent="center"
        paddingVertical="xxl"
      >
        <Button
          buttonStyle={styles.buttonStyle}
          titleStyle={styles.buttonTitleStyle}
          title="I'm ready"
          type="solid"
          onPress={() => {
            dispatch(
              updateOnboardingProccess({
                isClientReady: true,
              })
            );
            navigation.navigate('SignUpName');
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  descriptionStyle: {
    fontFamily: 'Poppins',
    fontWeight: '400',
    fontSize: 18,
    lineHeight: 28,
    color: colors.blue900,
    marginTop: 30,
  },
  buttonStyle: {
    borderRadius: 32,
    paddingHorizontal: 48,
    paddingVertical: 12,
    backgroundColor: colors.purple300,
  },
  buttonTitleStyle: { fontFamily: 'Poppins', fontSize: 16, lineHeight: 24 },
});

export default IntroSettingIntentions;
